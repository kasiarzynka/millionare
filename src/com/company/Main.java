package com.company;

/*
Java FX, Swing


 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;


//JFRAME klasa ktora tworzy okienko w Javie
//Main dziedziczy po JFRAMIE
//Main staje sie okienkiem
//poniewaz dostaje wszystkie pola i metody
//ktore posiada JFramme


public class Main extends JFrame implements ActionListener {

    //okno zawiera 2 buttony, i label

    //konstruktor klasy Main = domyslny ale nadpisywany

    private QuestionGenerator questionGenerator;

    public QuestionGenerator getQuestionGenerator() {
        return questionGenerator;

    }

    public void setQuestionGenerator(QuestionGenerator questionGenerator) {
        this.questionGenerator = questionGenerator;
    }

    private int currentQuestion;
    private int score =0;


    private List<Question> questionList = new ArrayList<>();

    public Main() {
        setQuestionGenerator(new SimpleQuestionGenerator());
        questionList = questionGenerator.generateQuestions();
        setSize(500, 500); //ustawia wielkosc okna
        //metoda setSize() znajduje sie w JFRAME

        setTitle("Millionaires"); //ustawia tytul okna
        //metoda znajduje sie w klasie JFRAME
        setVisible(true); //ustawiamy widoczne okno
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        //zamykanie okna metoda setDefaultCloseOperation
        //WindowsConstans.EXIT_ON_CLOSE
        //jest zmienna ktora jest w klasie WundowsCOnstans
        //i ma wartosc 3


        JButton yesButton = new JButton("TAK");
        yesButton.addActionListener(this); //sowkiem this mowimy ze jesli ja klikne tak
        // to obsluge za to klinkiecie przejmuje okienko
        JButton noButton = new JButton("NIE");
        noButton.addActionListener(this);
        label = new JLabel(questionList.get(currentQuestion).getContent(), 0);
        //0 wycentrowanie label
        // 3 rzedy 1 kolumna
        setLayout(new GridLayout(3, 1));

        //dodanie do okna pytanie o rzedzie numerze 1
        add(label);

        //dodanie do okna przycisku o rzedzie numer 2
        add(yesButton);

        //dodanie do okna przycisku o rzedzie nuemr 3
        add(noButton);
    }


    private JLabel label;

    public static void main(String[] args) {
        // write your code here
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Main main = new Main();

            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("uzytkownik przycisnal:");
        JButton button = (JButton) e.getSource();
        System.out.println(button.getText());


        boolean ourAnswer;
        if (button.getText().equals("TAK")){
            ourAnswer = true;
        } else {
            ourAnswer = false;
        }


        if (questionList.get(currentQuestion).isCorrect() == ourAnswer) {
            score++;
        }

        if (questionList.size() > currentQuestion+1){
            currentQuestion++;
            label.setText(questionList.get(currentQuestion).getContent());
        } else {
            button.setVisible(false);
            setLayout(new GridLayout());
            label.setText("Twoj wynik to: " + score +"/"+questionList.size());
            add(label);
        }


    }
}
