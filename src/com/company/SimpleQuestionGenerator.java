package com.company;

import java.util.ArrayList;
import java.util.List;

public class SimpleQuestionGenerator implements QuestionGenerator{

    @Override
    public List<Question> generateQuestions(){
        List<Question>questions = new ArrayList<>();

        questions.add(new Question("Czy Polska lezy w Europie",true));
        questions.add(new Question("2+2=5", false));
        questions.add(new Question("2 do potegi 10 to 1024?", true));
        questions.add(new Question("Czy Kapsztad to stolica Argentyny?", true));
        return questions;

    }

}
